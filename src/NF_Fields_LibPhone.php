<?php

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

 if (! defined('ABSPATH') ) { exit;
}

/**
 * Class NF_Fields_LibPhone
 */
class NF_Fields_LibPhone extends NF_Fields_Textbox
{
    protected $_name = 'libphone';

    protected $_nicename = 'Lib phone';

    protected $_section = 'userinfo';

    protected $_icon = 'phone';

    protected $_type = 'textbox';

    protected $_templates = 'libphone';

    const E164 = 'E164';
    const INTERNATIONAL = 'INTERNATIONAL';
    const NATIONAL = 'NATIONAL';
    const RFC3966 = 'RFC3966';

    protected $_settings_all_fields = array(
        'key', 'label', 'label_pos', 'required', 'default_country', 'format_phonenumber', 'classes', 'admin_label', 'help', 'description'
    );

    public function __construct()
    {
        parent::__construct();

        add_action('wp_footer', [$this, 'addTemplate']);
        add_action('admin_footer', [$this, 'addTemplate']);
        add_action('admin_footer', [$this, 'addAdminOptionsTemplate']);

        add_filter('ninja_forms_subs_export_field_value_' . $this->_name, array( $this, 'filterCsvValue' ), 10, 2);

        wp_enqueue_script(
            'libphonenumber-js',
            'https://cdnjs.cloudflare.com/ajax/libs/libphonenumber-js/1.10.6/libphonenumber-js.min.js',
            array()
        );
        wp_enqueue_script(
            'nf-libphonenumber-field',
            plugins_url('../js/libphonenumber-field.js', __FILE__),
            array('jquery', 'nf-front-end', 'libphonenumber-js')
        );

        $this->_nicename = esc_html__('Lib phone', 'ninja-forms-libphonenumber-field');

        $this->_settings[ 'custom_name_attribute' ][ 'value' ] = 'phone';
        $this->_settings[ 'personally_identifiable' ][ 'value' ] = '1';
    }

    public function validate( $field, $data ) 
    {
        $errors = parent::validate($field, $data);
        if (!empty($field['value'])) {
            $phoneUtil = PhoneNumberUtil::getInstance();

            try {
                $numberProto = $phoneUtil->parse($field['value'], $field['settings']['default_country']);

                if (!$phoneUtil->isValidNumber($numberProto)) {
                    $errors[] = esc_html__('Please enter a valid phone number.', 'ninja-forms-libphonenumber-field');
                }
            } catch (NumberParseException $th) {
                $errors[] = esc_html__('Please enter a valid phone number.', 'ninja-forms-libphonenumber-field');
            }
        }
        return $errors;
    }

    public function process( $field, $data )
    {
        if (isset($field['settings']['format_phonenumber']) && $field['settings']['format_phonenumber'] !== '') {
            $phoneUtil = PhoneNumberUtil::getInstance();
            $phoneFormat = $field['settings']['format_phonenumber'];
            try {
                $value = $data['fields'][$field['id']]['value'];
                $numberProto = $phoneUtil->parse($value, $field['settings']['default_country']);
                if ($phoneFormat === self::E164) {
                    $value = $phoneUtil->format($numberProto, PhoneNumberFormat::E164);
                }
                if ($phoneFormat === self::INTERNATIONAL) {
                    $value = $phoneUtil->format($numberProto, PhoneNumberFormat::INTERNATIONAL);
                }
                if ($phoneFormat === self::NATIONAL) {
                    $value = $phoneUtil->format($numberProto, PhoneNumberFormat::NATIONAL);
                }
                if ($phoneFormat === self::RFC3966) {
                    $value = $phoneUtil->format($numberProto, PhoneNumberFormat::RFC3966);
                }
                $data['fields'][$field['id']]['value'] = $value;
            } catch (NumberParseException $th) {
                // silence is bliss
            }
        }

        return $data;
    }

    public function filterCsvValue($fieldValue, $field)
    {
        /*
         * sanitize this in case someone tries to inject data that runs in
         * Excel and similar apps
         * */
        if (0 < strlen($fieldValue) ) {
            $first_char = substr($fieldValue, 0, 1);
            if (in_array($first_char, array( '=', '@', '+', '-' )) ) {
                return "'" . $fieldValue;
            }
        }

        return $fieldValue;
    }

    public function addTemplate()
    {
        echo <<<'TEMPLATE'
        <script id='tmpl-nf-field-libphone' type='text/template'>
            <input
                    type="tel"
                    value="{{{ data.value }}}"
                    class="{{{ data.renderClasses() }}} nf-element"

                    id="nf-field-{{{ data.id }}}"
                    <# if( ! data.disable_browser_autocompletes ){ #>
                    name="{{ data.custom_name_attribute || 'nf-field-' + data.id + '-' + data.type }}"
                    autocomplete="tel"
                    <# } else { #>
                    name="{{ data.custom_name_attribute || 'nf-field-' + data.id }}"
                    {{{ data.maybeDisableAutocomplete() }}}
                    <# } #>
                    {{{ data.renderPlaceholder() }}}

                    aria-invalid="false"
                    aria-describedby="nf-error-{{{ data.id }}}"
                    aria-labelledby="nf-label-field-{{{ data.id }}}"

                    {{{ data.maybeRequired() }}}
            >
        </script>
        TEMPLATE;
    }
}
