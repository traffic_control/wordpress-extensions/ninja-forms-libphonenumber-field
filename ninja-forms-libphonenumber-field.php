<?php

/**
 * Plugin Name: Ninja Forms libphonenumber field
 * Plugin URI: http://ninjaforms.com/?utm_source=Ninja+Forms+Plugin&utm_medium=readme
 * Description: Ninja forms email field is a ninja form plugin that implements google libphonenumber to validate field
 * Version: 3.0.0
 * Text Domain: ninja-forms-libphonenumber-field
 * Requires Plugins: ninja-forms
 * Domain Path: /lang/
 */

$registerPhoneField = function ($fields) {
    include_once 'src/NF_Fields_LibPhone.php';
    $fields['libphone'] = new NF_Fields_LibPhone;
    return $fields;
};

$registerPhoneOptions = function ($fields) {
    $fields['default_country'] = array(
        'name' => 'default_country',
        'type' => 'textbox',
        'label' => esc_html__('Default country', 'ninja-forms-libphonenumber-field'),
        'width' => 'one-half',
        'group' => 'primary',
        'value' => 'US',
        'help' => esc_html__('Enter default country code to validate phone number.', 'ninja-forms-libphonenumber-field'),
    );
    $fields['format_phonenumber'] = array(
        'name' => 'format_phonenumber',
        'type' => 'select',
        'options' => [
            [
                'label' => esc_html__('As-is', 'ninja-forms-libphonenumber-field'),
                'value' => null,
            ],
            [
                'label' => esc_html__('E.164', 'ninja-forms-libphonenumber-field'),
                'value' => 'E164',
            ],
            [
                'label' => esc_html__('International', 'ninja-forms-libphonenumber-field'),
                'value' => 'INTERNATIONAL',
            ],
            [
                'label' => esc_html__('National', 'ninja-forms-libphonenumber-field'),
                'value' => 'NATIONAL',
            ],
            [
                'label' => esc_html__('RFC 3966', 'ninja-forms-libphonenumber-field'),
                'value' => 'RFC3966',
            ],
        ],
        'label' => esc_html__('Format phonenumber', 'ninja-forms-libphonenumber-field'),
        'width' => 'one-half',
        'group' => 'primary',
        'value' => '',
        'help' => esc_html__('In what format should we save phonenumber in.', 'ninja-forms-libphonenumber-field'),
    );

    return $fields;
};

$registerTranslations = function ($translations) {
    $translations['libPhoneFieldValidationError'] = esc_html__('Please enter a valid phone number.', 'ninja-forms-libphonenumber-field');

    return $translations;
};

$registerDisplaySettings = function ($displaySettings)
{
    $displaySettings['custom_messages']['settings'][] = [
        'name' => 'libPhoneFieldValidationError',
        'type' => 'textbox',
        'label' => esc_html__('Please enter a valid phone number.', 'ninja-forms-libphonenumber-field'),
        'width' => 'full'
    ];

    return $displaySettings;
};

add_filter('ninja_forms_register_fields', $registerPhoneField);

add_filter('ninja_forms_field_settings', $registerPhoneOptions);

add_filter('ninja_forms_i18n_front_end', $registerTranslations);

add_filter('ninja_forms_form_display_settings', $registerDisplaySettings);

load_plugin_textdomain('ninja-forms-libphonenumber-field', false, basename(dirname(__FILE__)) . '/lang');