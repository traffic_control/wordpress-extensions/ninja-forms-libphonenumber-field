;(function (jQuery, document, window, undefined) {
    var errorId = 'libphonefield';

    var LibPhoneFieldController = Marionette.Object.extend({
        initialize: function () {
            var radioChannel = nfRadio.channel('libphone');
            this.listenTo(radioChannel, 'init:model', this.registerFunctions);
            this.listenTo(radioChannel, 'change:modelValue', this.onValueChange);
        },

        registerFunctions: function (model) {
            model.getValue = this.getValue;
        },

        onBlur: function (el, model) {
            var value = jQuery(el).val();
            this.phoneChanged(value, model);
        },
        onValueChange: function (model) {
            var value = model.get('value');
            this.phoneChanged(value, model);
        },
        phoneChanged: function (value, model) {
            var formModel = nfRadio.channel('app').request('get:form', model.get('formID'));

            var fieldId = model.get('id');
            if (value.length > 0) {
                var defaultCountry = model.get('default_country');
                try {
                    var parsedNumber = libphonenumber.parsePhoneNumber(value, defaultCountry);
                    if (parsedNumber.isValid()) {
                        nfRadio.channel('fields').request('remove:error', fieldId, errorId);
                        return ;
                    }
                    nfRadio.channel('fields').request('add:error', fieldId, errorId, formModel.get('settings').libPhoneFieldValidationError);
                } catch (error) {
                    nfRadio.channel('fields').request('add:error', fieldId, errorId, formModel.get('settings').libPhoneFieldValidationError);
                }
                return ;
            }
            nfRadio.channel('fields').request('remove:error', fieldId, errorId);
        },
        formatValue: function (model, value) {
            var defaultCountry = model.get('default_country');
            var format = model.get('format_phonenumber');

            var parsedNumber = libphonenumber.parsePhoneNumber(value, defaultCountry);

            if (format === 'E164') {
                return parsedNumber.format('E.164');
            }
            if (format === 'INTERNATIONAL') {
                return parsedNumber.format('INTERNATIONAL');
            }
            if (format === 'NATIONAL') {
                return parsedNumber.format('NATIONAL');
            }
            if (format === 'RFC3966') {
                return parsedNumber.format('RFC3966');
            }

            return currentValue;
        },

        getValue: function () {
            var currentValue = model.get('value');
            return this.formatValue(model, currentValue)
        }
    });

    jQuery(document).ready(function () {
        return new LibPhoneFieldController()
    })
})(jQuery, document, window);